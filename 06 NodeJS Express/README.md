 NodeJS + Express + Mongoose

* start: 	localhost:5554     			for Ajax and jQuery 
* -		localhost:5554/sockets     	for using sockets
* chat history stores  in database chatMongoDB ( using MongoDB)

#########
Реализовать чат с помощью AJAX. На странице предусмотреть поле ввода имени пользователя и сообщения. По нажатию кнопки отправки данные отправляются на бекенд по AJAX. При перезагрузке страницы чат показывает сообщения в хронологическом порядке.

Повторить реализацию чата с использованием сокетов (без перезагрузки страницы). Фронтенд - на чем угодно. Бекенд - nodejs + socketio. Необходимо, чтобы сообщения посылались и принимались, даже если открыто несколько вкладок с приложением.

Опираться на лекцию по Node.js и лекцию Web Transports (по сути реализовать пример из видео и посмотреть вживую как это работает).

Хранилище данных любое. Если будете использовать MongoDB, можно вопросы как это делать писать в скайп dmitriy.b_binary
 
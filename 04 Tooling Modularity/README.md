"#  Tooling Modularity

# Installation
```
npm install
gulp
```
open http://localhost:47/


#########################
Відредагувати файли gulpfile.js і src/js/app.js. В них є коментарі що потрібно робити:
реалізувати таск sass - повинен перекомпільовувати .sass файли з папки src/sass в .css файли з переміщенням в папку dist/css
для таску requireJS добавити мініфікацію (uglification) вихідного файлу bundle.js
для таску watch добавити спостереження за файлами в папках src/js і src/sass з виконанням відповідних тасків
в файлі src/js/app.js викликати функцію run модуля secondModule з аргументом jsOutput
При редагуванні будь-яких файлів в папці src gulp повинен автоматично запускати потрібні таски і перезавантажувати сторінку.

В результаті готова веб сторінка повинна виглядати как на картинке

